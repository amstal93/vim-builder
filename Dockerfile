FROM ubuntu:18.04 AS builder
RUN sed -i -e '/^# deb-src .* main/s/^# //' /etc/apt/sources.list && apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get build-dep -y vim
RUN apt-get install -y git ca-certificates gawk
WORKDIR /build
RUN git clone https://github.com/vim/vim.git
WORKDIR /build/vim
COPY build.sh .
RUN ./build.sh

FROM ubuntu:18.04 AS plugins
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install \
        curl \
        git \
        unzip \
        stow \
        exuberant-ctags \
        vim-gtk \
    && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/stow/vim /usr/local/stow/vim
RUN stow --dir /usr/local/stow vim
COPY install-plugins .
RUN ./install-plugins
WORKDIR /root
